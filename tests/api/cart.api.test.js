const { test, expect } = require("@playwright/test");
const { StatusCodes } = require("http-status-codes");
const { getAuthToken, getProductId, getProductBody, getCartId, getCartBody } = require("../../lib/helpers");
const { POST_SUCCESS, DELETE_SUCCESS } = require("serverest/src/utils/constants");

test.describe.parallel('Carts API', () => {

  let authorization

  test.beforeAll(async ({ request }) => {
    authorization = await getAuthToken(request)
  })

  test('retrieves the list of carts', async ({ request }) => {
    const response = await request.get('/carrinhos')

    expect(response.status()).toEqual(StatusCodes.OK)
  })

  test('retrieves an existing cart by its id', async ({ request }) => {
    const productId = await getProductId(request, authorization, getProductBody())
    const cartId = await getCartId(request, authorization, getCartBody(productId))

    const response = await request.get(`/carrinhos/${cartId}`)

    expect(response.status()).toEqual(StatusCodes.OK)
  })

  test('creates a cart successfully', async ({ request }) => {
    const productId = await getProductId(request, authorization, getProductBody())
    const response = await request.post('/carrinhos', {
      data: getCartBody(productId),
      headers: { 'Authorization': authorization }
    })
    const cart = await response.json()

    expect(response.status()).toEqual(StatusCodes.CREATED)
    expect(cart.message).toEqual(POST_SUCCESS)
  })

  test('delets an order successfully', async ({ request }) => {
    const productId = await getProductId(request, authorization, getProductBody())
    await getCartId(request, authorization, getCartBody(productId))

    const response = await request.delete('/carrinhos/concluir-compra', {
      headers: { 'Authorization': authorization }
    })

    const cart = await response.json()

    expect(response.status()).toEqual(StatusCodes.OK)
    expect(cart.message).toEqual(DELETE_SUCCESS)
  })
});
